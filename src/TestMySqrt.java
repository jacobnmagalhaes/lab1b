
//Author: jacob magalhaes
//Student #: 29737137
//date: 09 09 2014
//Lab : L1C
//Purpose: To test the function MySqrt
import java.util.Scanner;


public class TestMySqrt {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
	
		Scanner scanner = new Scanner (System.in);
		scanner.useDelimiter("\r\n");
		
		
		
		
		System.out.println("What do you want to square root: ");
		double n = scanner.nextDouble();
		
		scanner.close();
		
		int r = 1;
		for ( r = 1; n > r; --n){
		double result = MySqrt.sqrt( n);
		System.out.println(" This is said square root: " + result);
		}
	}

}
