
This is the test explanation:

 This is said square root: 4.0
 This is said square root: -3.872983346207417
 This is said square root: 3.7416573867739413
 This is said square root: -3.605551275463989
 This is said square root: 3.4641016151377544
 This is said square root: -3.3166247903554
 This is said square root: 3.1622776601683795
 This is said square root: -3.0
 This is said square root: 2.8284271247461903
 This is said square root: -2.6457513110645907
 This is said square root: 2.449489742783178
 This is said square root: -2.23606797749979
 This is said square root: 2.0
 This is said square root: -1.7320508075688772
 This is said square root: 1.4142135623730951
 
 Upon extreme testing it showed that numbers would alternate between positive and negative values when called. Knowing that this is a 
 square root function, even though it recalls the same number it could be positive or negative, so this is somewhat expected. However
 coder would need to assign another parameter to specify if it is postive or negative. 
 
 This confused me in the beginning but when i thought there might be something wrong with the code i deced to create a for loop to test 
 it more extensively.
 Before creating that loop I had just tested a few numbers and was getting positive results.
 
 Regards Jacob M.